pragma solidity >=0.5.0 <0.7.0;

import {EaeForm as eaeForm} from "./beans/EaeForm.sol";
import {EaeUser as eaeUser} from "./beans/EaeUser.sol";

/*
 * Model contract
 */
contract EaeModel {
    eaeUser.User[] public Users;

    uint public idUser;

    // add new user
    function addNewUser (address _address, string memory _role, string memory _profile) private {
        if ( !isUserKnown (_address) ) {
            Users.push(eaeUser.User({
                idUser:idUser++,
                role:_role,
                profile:_profile,
                ethAddress:_address
            }));
        }
    }

    function getUser ( address _address ) internal view returns (eaeUser.User memory user) {
        for (uint i = 0; i < Users.length; i++) {
            if ( Users[i].ethAddress == _address ) {
                user = Users[i];
            }
        }
        return user;
    }

    function isUserKnown ( address _address ) internal view returns (bool isKnown) {
        isKnown = false;
        for (uint i = 0; i < Users.length; i++) {
            if ( Users[i].ethAddress == _address ) {
                isKnown = true;
            }
        }
        return isKnown;
    }

    // create new form for a specific user

    // valid form by user

    // sign form by user

    // consult form by user
}