pragma solidity >=0.5.0 <0.7.0;

import { EaeFormDomain as eaeFormDomain } from "./EaeFormDomain.sol";

contract EaeForm {
    struct Form {
        string idForm;
        string label;
        string dtCreated;
        string dtClosed;
        string dtHistoricized;
        string idOwner;
        string idManager;
        string dtOwnerValidated;
        string dtManagerValidated;
        string dtOwnerSigned;
        string dtManagerSigned;
        string status;
        mapping(uint => eaeFormDomain.FormDomain) domains;
    }
    constructor () public {
    }
}