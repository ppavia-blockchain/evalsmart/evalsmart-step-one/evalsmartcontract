pragma solidity >=0.5.0 <0.7.0;

import { EaeFormQuery as eaeFormQuery } from "./EaeFormQuery.sol";

contract EaeFormDomain {
    struct FormDomain {
        string idDomain;
        string label;
        string idForm;
        mapping(uint => eaeFormQuery.FormQuery) queries;
    }
    constructor () public {
    }
}