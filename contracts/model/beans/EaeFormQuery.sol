pragma solidity >=0.5.0 <0.7.0;

import { EaeFormResponse as eaeFormResponse } from "./EaeFormResponse.sol";

contract EaeFormQuery {
    struct FormQuery {
        string idQuery;
        string label;
        string idDomain;
        mapping(uint => eaeFormResponse.FormResponse) responses;
    }
    constructor () public {
    }
}