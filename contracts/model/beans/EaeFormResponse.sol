pragma solidity >=0.5.0 <0.7.0;

contract EaeFormResponse {
    struct FormResponse {
        string idResponse;
        string label;
        string idQuery;
        string idOwner;
        string responseType;
        string content;
    }
    constructor () public {
    }
}