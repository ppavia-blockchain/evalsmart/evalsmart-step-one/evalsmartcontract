pragma solidity >=0.5.0 <0.7.0;

contract EaeFormStatus {
    enum FormStatus {
        CREATED,
        VALIDATED,
        SIGNED,
        EXPIRED,
        ARCHIVED,
        DELETED
    }
    constructor () public {
    }
}