pragma solidity >=0.5.0 <0.7.0;

import { EaeForm as eaeForm } from "./EaeForm.sol";

contract EaeUser {
    struct User {
        uint idUser;
        string role;
        string profile;
        address ethAddress;
    }

    constructor () public {
    }
}