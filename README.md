# SOLIDITY

## Visibility and Getters

### Functions :
`external` :  
External functions are part of the contract interface, which means they can be called from other contracts and via transactions. An external function f cannot be called internally (i.e. f() does not work, but this.f() works). External functions are sometimes more efficient when they receive large arrays of data.  

`public`  
Public functions are part of the contract interface and can be either called internally or via messages. For public state variables, an automatic getter function (see below) is generated.

`internal`  
Those functions and state variables can only be accessed internally (i.e. from within the current contract or contracts deriving from it), without using `this`.  

`private`   
Private functions and state variables are only visible for the contract they are defined in and not in derived contracts.

`modifier`  
Modifiers can be used to easily change the behaviour of functions. For example, they can automatically check a condition prior to executing the function. Modifiers are inheritable properties of contracts and may be overridden by derived contracts.

`view`  
Function declared with `view` promises not to modify the state.

`pure`  
Function declared with `pure` promises not to read from or modify the state.

#### parameters

#### return values
function can returns mutiple values :  
`return (uint, string)`

### Variables :
`constant`
